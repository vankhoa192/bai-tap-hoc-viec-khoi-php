$(document).ready(function () {
    size_section = $("#myList section").size();
    x=3;
    $('#myList section:lt('+x+')').show();
    $('#loadMore').click(function () {
        x= (x+2 <= size_section) ? x+2 : size_section;
        $('#myList section:lt('+x+')').show();
    });
    $('#showLess').click(function () {
        x=(x-2<0) ? 3 : x-2;
        $('#myList section').not(':lt('+x+')').hide();
    });
});